/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package shape;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class ShapeSimulation {
    public static void main (String[] args) {
        Circle c = new Circle(8);
        Square s = new Square(6);
        
        System.out.println("The Area of the Circle is " + c.getArea() + " and the Perimeter of the Circle is " + c.getPerimeter());
        System.out.println("The Area of the Square is " + s.getArea() + " and the Perimeter of the Square is " + s.getPerimeter());
    }
}
