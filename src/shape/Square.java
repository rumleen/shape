/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package shape;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Square extends Shape {
    private double length;
    
    public Square (double length){
        this.length = length;
    }
    
    @Override
    public double getArea(){
        return length*length;
    }
    
    @Override
    public double getPerimeter() {
        return 4*length;
    }
}
